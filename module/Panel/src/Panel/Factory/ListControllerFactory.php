<?php
/**
 * 26.03.2015
 */
namespace Panel\Factory;

use Panel\Controller\ListController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Создание контроллера с сервисом заявок
 *
 * @author Ilia Garaga <ilia at suo>
 */
class ListControllerFactory implements FactoryInterface
{
    /**
     * Создание сервиса
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return ListController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator = $serviceLocator->getServiceLocator();
        $ticketService      = $realServiceLocator->get('Panel\Service\TicketServiceInterface');

        return new ListController($ticketService);
    }

}
