<?php
/**
 * 26.03.2015
 */
namespace Panel\Factory;

use Panel\Service\TicketService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of TicketServiceFactory
 *
 * @author Ilia Garaga <ilia at suo>
 */
class TicketServiceFactory implements FactoryInterface
{
    /**
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return TicketService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new TicketService(
                $serviceLocator->get('Panel\Mapper\TicketMapperInterface')
            );
    }

}
