<?php
/*
 * 26.03.2015
 */

namespace Panel\Controller;

use Panel\Service\TicketServiceInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ListController extends AbstractActionController
{
    /**
     *
     * @var \Panel\Service\TicketServiceInterface
     */
    protected $ticketService;

    public function __construct(TicketServiceInterface $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    public function indexAction()
    {
        return new ViewModel(
                array(
                    'tickets' => $this->ticketService->findAllTickets()
                )
        );
    }

}