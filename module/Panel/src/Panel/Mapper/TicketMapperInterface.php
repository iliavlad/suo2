<?php
/**
 * 26.03.2015
 */
namespace Panel\Mapper;

use Panel\Model\PostInterface;

/**
 *
 * @author Ilia Garaga <ilia at suo>
 */
interface TicketMapperInterface
{
    /**
     * @return array|PanelInterface[]
     */
    public function findAll();
}
