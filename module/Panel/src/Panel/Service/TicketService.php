<?php
/**
 * 26.03.2015
 */

namespace Panel\Service;

use Panel\Model\Ticket;
use Panel\Mapper\TicketMapperInterface;

class TicketService implements TicketServiceInterface
{
    /**
     *
     * @var \Panel\Mapper\TicketMapperInterface;
     */
    protected $ticketMapper;

    /**
     *
     * @param TicketMapperInterface $ticketMapper
     */
    public function __construct(TicketMapperInterface $ticketMapper)
    {
        $this->ticketMapper = $ticketMapper;
    }

//    protected $data = array(
//         array(
//             'id'    => 1,
//             'room_id' => 11,
//             'ticket_number'  => 'This is our first blog post!'
//         ),
//         array(
//             'id'     => 2,
//             'room_id' => 22,
//             'ticket_number'  => 'This is our second blog post!'
//         ),
//         array(
//             'id'     => 3,
//             'room_id' => 33,
//             'ticket_number'  => 'This is our third blog post!'
//         ),
//         array(
//             'id'     => 4,
//             'room_id' => 44,
//             'ticket_number'  => 'This is our fourth blog post!'
//         ),
//         array(
//             'id'     => 5,
//             'room_id' => 55,
//             'ticket_number'  => 'This is our fifth blog post!'
//         )
//     );

    public function findAllTickets()
    {
//        $tickets = array();
//
//        foreach ($this->data as $data)
//        {
//            $ticket = new Ticket;
//            $ticket->setId($data['id']);
//            $ticket->setRoomId($data['room_id']);
//            $ticket->setTicketNumber($data['ticket_number']);
//
//            $tickets[] = $ticket;
//        }

        return $this->ticketMapper->findAll();
    }
}