<?php
/*
 * 26.03.2105
 */

namespace Panel\Service;

use Panel\Model\TicketInterface;

interface TicketServiceInterface
{
     /**
      * Возвращает список заявок. Каждая заявка в списке реализует \Panel\Model\TicketInterface
      *
      * @return array|TicketInterface[]
      */
     public function findAllTickets();
}