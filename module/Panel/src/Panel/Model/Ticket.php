<?php
/**
 * 26.03.2015
 *
 * @author Ilia Garaga <ilia at suo>
 */

namespace Panel\Model;

class Ticket implements TicketInterface
{
    /**
     * Ид заявки
     *
     * @var int
     */
    protected $id;

    /**
     * Ид кабинета
     *
     * @var int
     */
    protected $room_id;

    /**
     * Номер заявки
     *
     * @var string
     */
    protected $ticket_number;

    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     *
     * @param int $room_id
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
    }

    public function getTicketNumber()
    {
        return $this->ticket_number;
    }

    /**
     * 
     * @param string $ticket_number
     */
    public function setTicketNumber($ticket_number)
    {
        $this->ticket_number = $ticket_number;
    }

}
