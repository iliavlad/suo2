<?php
/**
 * 26.03.2105
 *
 * @author Ilia Garaga <ilia at suo>
 */

namespace Panel\Model;

interface TicketInterface
{
    /**
     * Возвращает ид заявки
     *
     * @return int
     */
    public function getId();

    /**
     * Возвращает id кабинета
     *
     * @return int
     */
    public function getRoomId();

    /**
     * Возвращает номер заявки
     *
     * @return string
     */
    public function getTicketNumber();
}
